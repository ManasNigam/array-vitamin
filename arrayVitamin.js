const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];




//     1. Get all items that are available 



//     const allAvailableItems = items.filter(function(items){
//         if(items.available == true){
//             return true;
//         }
//         else{
//             return false;
//         }
//     });

//    console.log(allAvailableItems);

// --------------------------------------------------------------------------------

//   Get all items containing only Vitamin C.

// const allVitaminCitems = items.filter(function(items){
//     if(items.contains == "Vitamin C"){
//         return true;
//     }
//     else{
//         return false;
//     }
// });

// console.log(allVitaminCitems);

// ---------------------------------------------------------------------------------


// 3. Get all items containing Vitamin A.

// const allVitaminAitems = items.filter(function(items){
//     if((items.contains).includes("Vitamin A")){
//         return true;
//     }
//     else{
//         return false;
//     }
// });

// console.log(allVitaminAitems);


// ----------------------------------------------------------------------------------

// Group items based on the Vitamins that they contain in the following format:
//         {
//             "Vitamin C": ["Orange", "Mango"],
//             "Vitamin K": ["Mango"],
//         }


const getnams = (Vitamin)=>{
    let arr = []
const allVitaminAitems = items.filter(function(items){
        if((items.contains).includes(Vitamin)){
                arr.push(items.name)
        }
        else{
            return false;
        }
    });
    return arr;
}

let group = {}

function names() {
    group["Vitamin A"] = getnams("Vitamin A")
    group["Vitamin B"] = getnams("Vitamin B")
    group["Vitamin C"] = getnams("Vitamin C")
    group["Vitamin D"] = getnams("Vitamin D")
    group["Vitamin K"] = getnams("Vitamin K")
}
names();

console.log(group);

// ----------------------------------------------------------------------------------

// 5. Sort items based on number of Vitamins they contain.

// items.sort(function(a,b){

//     if(a.contains.length < b.contains.length){
//         return 1;
//     }
//     else{
//         return -1;
//     }
// })
// console.log(items);
